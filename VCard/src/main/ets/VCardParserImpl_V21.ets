/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { LineReaderInterface } from "./PlatformInterface/LineReaderInterface"
import { VCardConfig } from "./VCardConfig"
import { VCardParser_V21 } from "./VCardParser_V21"
import { Utf8ArrayToStr } from "./utils/Utf"
import {StringBuilder} from './utils/HOSStringBuilder';
import { PlatformFactory } from "./PlatformInterface/PlatformFactory"
import { VCardUtils} from "./utils/VCardUtils"
import { Log } from "./Log"
import { VCardInterpreter } from "./VCardInterpreter"
import { VCardProperty } from "./VCardProperty"
import { VCardParser } from "./VCardParser"
import { VCardConstants } from "./VCardConstants"

export class VCardParserImpl_V21 extends VCardParser {
    private LOG_TAG = "VCardParserImpl_V21"

    private static DEFAULT_ENCODING = '8BIT'
    private static DEFAULT_CHARSET = 'UTF-8'

    private mIntermediateCharset: string
    private mInterpreterList: Array<VCardInterpreter> = new Array()
    private mCanceled: boolean = false

    /**
    * <p>
    * The encoding type for deconding byte streams. This member variable is
    * reset to a default encoding every time when a new item comes.
    * </p>
    * <p>
    * "Encoding" in vCard is different from "Charset". It is mainly used for
    * addresses, notes, images. "7BIT", "8BIT", "BASE64", and
    * "QUOTED-PRINTABLE" are known examples.
    * </p>
    */
    protected mCurrentEncoding?: string 

    protected mCurrentCharset?: string 

    protected mReader?: LineReaderInterface 

    protected mUnknownTypeSet = new Set<string>()

    protected mUnknownValueSet = new Set<string>();

    constructor(type?: number) {
        super()
        this.mIntermediateCharset = VCardConfig.DEFAULT_INTERMEDIATE_CHARSET

    }

    protected isValidPropertyName(propertyName: string): boolean {
        if (!(this.getKnownPropertyNameSet().has(propertyName.toUpperCase()) ||
            propertyName.startsWith("X-"))
            && !this.mUnknownTypeSet.has(propertyName)) {
            this.mUnknownTypeSet.add(propertyName);
            Log.w(this.LOG_TAG, "Property name unsupported by vCard 2.1: " + propertyName);
        }
        return true;
    }

    protected getLine(): string | null {
        let line = this.mReader!.nextLine();
        Log.e(this.LOG_TAG,"getLine:"+line)
        return line;
    }

    protected peekLine(): string | null {
        let line = this.mReader!.peekLine();
        Log.e(this.LOG_TAG,"peekLine:"+line)
        return line
    }

    protected getNonEmptyLine(): string {
        let line: string | null;
        while (true) {
            line = this.getLine();
            if (line == null) {
                throw new Error("Reached end of buffer.");
            } else if (line.trim().length > 0) {
                return line;
            }
        }
    }

    private parseOneVCard(): boolean {
        Log.e(this.LOG_TAG,"parseOneCard A")
        // reset for this entire vCard.
        this.mCurrentEncoding = VCardParserImpl_V21.DEFAULT_ENCODING;
        this.mCurrentCharset = VCardParserImpl_V21.DEFAULT_CHARSET;

        // allow parsing of vcards that have mime data leading up to BEGIN:VCARD
        let allowGarbage = true;
        if (!this.readBeginVCard(allowGarbage)) {
            return false;
        }
        this.mInterpreterList.forEach((vcardInterpreter, index, array) => {
            vcardInterpreter.onEntryStarted()
        })

        Log.e(this.LOG_TAG,"parseOneCard B")
        this.parseItems();
        this.mInterpreterList.forEach((vcardInterpreter, index, array) => {
            vcardInterpreter.onEntryEnded()
        })
        Log.e(this.LOG_TAG,"parseOneCard C")

        return true;
    }

    /**
    * @return True when successful. False when reaching the end of line
    * @throws IOException
    * @throws VCardException
    */
    protected readBeginVCard(allowGarbage: boolean): boolean {
        // TODO: use consructPropertyLine().
        let line: string | null;
        do {
            while (true) {
                line = this.getLine();
                if (line == null) {
                    return false;
                } else if (line.trim().length > 0) {
                    break;
                }
            }
            const strArray: Array<string> = line.split(":", 2);
            const length: number = strArray.length;

            // Although vCard 2.1/3.0 specification does not allow lower cases,
            // we found vCard file emitted by some external vCard expoter have such
            // invalid strings.
            // e.g. BEGIN:vCard
            if (length == 2 && strArray[0].trim().toLowerCase() == ("BEGIN").toLowerCase()
                && strArray[1].trim().toLowerCase() == ("VCARD").toLowerCase()) {
                return true
            } else if (!allowGarbage) {
                throw new Error("Expected string \"BEGIN:VCARD\" did not come "
                    + "(Instead, \"" + line + "\" came)");
            }
        } while (allowGarbage);

        throw new Error("Reached where must not be reached.");
    }

    /**
    * Parses lines other than the first "BEGIN:VCARD". Takes care of "END:VCARD"n and
    * "BEGIN:VCARD" in nested vCard.
    */
    /*
     * items = *CRLF item / item
     *
     * Note: BEGIN/END aren't include in the original spec while this method handles them.
     */
    protected parseItems(): void {
        let ended = false;

        try {
            ended = this.parseItem();
        } catch (e) {
            Log.w(this.LOG_TAG, "Invalid line which looks like some comment was found. Ignored." + e);
        }

        while (!ended) {
            try {
                ended = this.parseItem();
            } catch (e) {
                Log.e(this.LOG_TAG, "Invalid line which looks like some comment was found. Ignored. second -->"+ e);
            }
        }
    }

    /*
    * item = [groups "."] name [params] ":" value CRLF / [groups "."] "ADR"
    * [params] ":" addressparts CRLF / [groups "."] "ORG" [params] ":" orgparts
    * CRLF / [groups "."] "N" [params] ":" nameparts CRLF / [groups "."]
    * "AGENT" [params] ":" vcard CRLF
    */
    protected parseItem(): boolean {
        // Reset for an item.
        this.mCurrentEncoding = VCardParserImpl_V21.DEFAULT_ENCODING;

        const line = this.getNonEmptyLine();
        const propertyData: VCardProperty = this.constructPropertyData(line);

        const propertyNameUpper: string = propertyData.getName()!.toUpperCase();
        const propertyRawValue: string | null = propertyData.getRawValue();

        Log.e("parseItem propertyName:"+ propertyNameUpper +"   propertyRawValue:"+propertyRawValue +"  propertyData:"+propertyData )

        if (propertyNameUpper == (VCardConstants.PROPERTY_BEGIN)) {
            if (propertyRawValue!.toUpperCase() == ("VCARD")) {
                this.handleNest();
            } else {
                throw Error("Unknown BEGIN type: " + propertyRawValue);
            }
        } else if (propertyNameUpper.toUpperCase() == (VCardConstants.PROPERTY_END.toUpperCase())) {
            Log.e("parseItem END A")
            if (propertyRawValue!.toUpperCase() == ("VCARD")) {
                Log.e("parseItem END B")
                return true;  // Ended.
            } else {
                throw new Error("Unknown END type: " + propertyRawValue);
            }
        } else {
            this.parseItemInter(propertyData, propertyNameUpper);
        }
        return false;
    }

    private parseItemInter(property: VCardProperty, propertyNameUpper: string): void {
        let propertyRawValue: string = property.getRawValue()!;
        if (propertyNameUpper.toUpperCase() == (VCardConstants.PROPERTY_AGENT.toUpperCase())) {
            this.handleAgent(property);
        } else if (this.isValidPropertyName(propertyNameUpper)) {
            if (propertyNameUpper.toUpperCase() == (VCardConstants.PROPERTY_VERSION.toUpperCase()) &&
                !(propertyRawValue.toUpperCase() == (this.getVersionstring().toUpperCase()))) {
                throw new Error(
                    "Incompatible version: " + propertyRawValue + " != " + this.getVersionstring());
            }
            this.handlePropertyValue(property, propertyNameUpper);
        } else {
            throw new Error("Unknown property name: \"" + propertyNameUpper + "\"");
        }
    }

    private handleNest(): void {

        this.mInterpreterList.forEach((vcardInterpreter, index, array) => {
            vcardInterpreter.onEntryStarted()
        })
        this.parseItems();
        this.mInterpreterList.forEach((vcardInterpreter, index, array) => {
            vcardInterpreter.onEntryEnded()
        })
    }

    private static readonly STATE_GROUP_OR_PROPERTY_NAME: number = 0;
    private static readonly STATE_PARAMS: number = 1;
    // vCard 3.0 specification allows double-quoted parameters, while vCard 2.1 does not.
    private static readonly STATE_PARAMS_IN_DQUOTE: number = 2;

    protected constructPropertyData(line: string): VCardProperty {
        const propertyData: VCardProperty = new VCardProperty();

        const length: number = line.length;
        if (length > 0 && line.charAt(0) == '#') {
            throw new Error("VCardInvalidCommentLineException");
        }

        let state: number = VCardParserImpl_V21.STATE_GROUP_OR_PROPERTY_NAME;
        let nameIndex: number = 0;

        // This loop is developed so that we don't have to take care of bottle neck here.
        // Refactor carefully when you need to do so.
        for (let i = 0; i < length; i++) {
            const ch: string = line.charAt(i);
            switch (state) {
                case VCardParserImpl_V21.STATE_GROUP_OR_PROPERTY_NAME: {
                    if (ch == ':') {  // End of a property name.
                        const propertyName: string = line.substring(nameIndex, i);
                        propertyData.setName(propertyName);
                        propertyData.setRawValue(i < length - 1 ? line.substring(i + 1) : "");
                        return propertyData;
                    } else if (ch == '.') {  // Each group is followed by the dot.
                        const groupName: string = line.substring(nameIndex, i);
                        if (groupName.length == 0) {
                            Log.w(this.LOG_TAG, "Empty group found. Ignoring.");
                        } else {
                            propertyData.addGroup(groupName);
                        }
                        nameIndex = i + 1;  // Next should be another group or a property name.
                    } else if (ch == ';') {  // End of property name and beginneng of parameters.
                        const propertyName: string = line.substring(nameIndex, i);
                        propertyData.setName ( propertyName);
                        nameIndex = i + 1;
                        state = VCardParserImpl_V21.STATE_PARAMS;  // Start parameter parsing.
                    }
                    // TODO: comma support (in vCard 3.0 and 4.0).
                    break;
                }
                case VCardParserImpl_V21.STATE_PARAMS: {
                    if (ch == '"') {
                        if (VCardConstants.VERSION_V21.toLowerCase() == (this.getVersionstring().toLowerCase())) {
                            Log.w(this.LOG_TAG, "Double-quoted params found in vCard 2.1. " +
                                "Silently allow it");
                        }
                        state = VCardParserImpl_V21.STATE_PARAMS_IN_DQUOTE;
                    } else if (ch == ';') {  // Starts another param.
                        this.handleParams(propertyData, line.substring(nameIndex, i));
                        nameIndex = i + 1;
                    } else if (ch == ':') {  // End of param and beginenning of values.
                        this.handleParams(propertyData, line.substring(nameIndex, i));
                        propertyData.setRawValue(i < length - 1 ? line.substring(i + 1) : "");
                        return propertyData;
                    }
                    break;
                }
                case VCardParserImpl_V21.STATE_PARAMS_IN_DQUOTE: {
                    if (ch == '"') {
                        if (VCardConstants.VERSION_V21.toLowerCase() == (this.getVersionstring().toLowerCase())) {
                            Log.w(this.LOG_TAG, "Double-quoted params found in vCard 2.1. " +
                                "Silently allow it");
                        }
                        state = VCardParserImpl_V21.STATE_PARAMS;
                    }
                    break;
                }
            }
        }

        throw new Error("Invalid line: \"" + line + "\"");
    }

    /*
  * params = ";" [ws] paramlist paramlist = paramlist [ws] ";" [ws] param /
  * param param = "TYPE" [ws] "=" [ws] ptypeval / "VALUE" [ws] "=" [ws]
  * pvalueval / "ENCODING" [ws] "=" [ws] pencodingval / "CHARSET" [ws] "="
  * [ws] charsetval / "LANGUAGE" [ws] "=" [ws] langval / "X-" word [ws] "="
  * [ws] word / knowntype
  */
    protected handleParams(propertyData: VCardProperty, params: string): void {
        const strArray: string[] = params.split("=", 2);
        if (strArray.length == 2) {
            const paramName: string = strArray[0].trim().toUpperCase();
            let paramValue: string = strArray[1].trim();
            if (paramName == ("TYPE")) {
                this.handleType(propertyData, paramValue);
            } else if (paramName == ("VALUE")) {
                this.handleValue(propertyData, paramValue);
            } else if (paramName == ("ENCODING")) {
                this.handleEncoding(propertyData, paramValue.toUpperCase());
            } else if (paramName == ("CHARSET")) {
                this.handleCharset(propertyData, paramValue);
            } else if (paramName == ("LANGUAGE")) {
                this.handleLanguage(propertyData, paramValue);
            } else if (paramName.startsWith("X-")) {
                this.handleAnyParam(propertyData, paramName, paramValue);
            } else {
                throw new Error("Unknown type \"" + paramName + "\"");
            }
        } else {
            this.handleParamWithoutName(propertyData, strArray[0]);
        }
    }

    /**
     * vCard 3.0 parser implementation may throw VCardException.
     */
    protected handleParamWithoutName(propertyData: VCardProperty, paramValue: string): void {
        this.handleType(propertyData, paramValue);
    }

    /*
     * ptypeval = knowntype / "X-" word
     */
    protected handleType(propertyData: VCardProperty, ptypeval: string): void {
        if (!(this.getKnownTypeSet().has(ptypeval.toUpperCase())
            || ptypeval.startsWith("X-"))
            && !this.mUnknownTypeSet.has(ptypeval)) {
            this.mUnknownTypeSet.add(ptypeval);
            Log.w(this.LOG_TAG, `TYPE ${this.getVersion()}unsupported by %s: ${ptypeval}`);
        }
        propertyData.addParameter(VCardConstants.PARAM_TYPE, ptypeval);
    }

    /*
     * pvalueval = "INLINE" / "URL" / "CONTENT-ID" / "CID" / "X-" word
     */
    protected handleValue(propertyData: VCardProperty, pvalueval: string): void {
        if (!(this.getKnownValueSet().has(pvalueval.toUpperCase())
            || pvalueval.startsWith("X-")
            || this.mUnknownValueSet.has(pvalueval))) {
            this.mUnknownValueSet.add(pvalueval);
            Log.w(this.LOG_TAG,
                `The value ${this.getVersion()} unsupported by TYPE of %s:${pvalueval} `);
        }
        propertyData.addParameter(VCardConstants.PARAM_VALUE, pvalueval);
    }

    /*
     * pencodingval = "7BIT" / "8BIT" / "QUOTED-PRINTABLE" / "BASE64" / "X-" word
     */
    protected handleEncoding(propertyData: VCardProperty, pencodingval: string): void {
        if (this.getAvailableEncodingSet().has(pencodingval) ||
            pencodingval.startsWith("X-")) {
            propertyData.addParameter(VCardConstants.PARAM_ENCODING, pencodingval);
            // Update encoding right away, as this is needed to understanding other params.
            this.mCurrentEncoding = pencodingval.toUpperCase();
        } else {
            throw new Error("Unknown encoding \"" + pencodingval + "\"");
        }
    }

    /**
   * <p>
   * vCard 2.1 specification only allows us-ascii and iso-8859-xxx (See RFC 1521),
   * but recent vCard files often contain other charset like UTF-8, SHIFT_JIS, etc.
   * We allow any charset.
   * </p>
   */
    protected handleCharset(propertyData: VCardProperty, charsetval: string): void {
        this.mCurrentCharset = charsetval;
        propertyData.addParameter(VCardConstants.PARAM_CHARSET, charsetval);
    }

    /**
     * See also Section 7.1 of RFC 1521
     */
    protected handleLanguage(propertyData: VCardProperty, langval: string)
        : void {
        let strArray: string[] = langval.split("-");
        if (strArray.length != 2) {
            throw new Error("Invalid Language: \"" + langval + "\"");
        }
        let tmp: string = strArray[0];
        let length: number = tmp.length;
        for (let i = 0; i < length; i++) {
            if (!this.isAsciiLetter(tmp.charAt(i))) {
                throw new Error("Invalid Language: \"" + langval + "\"");
            }
        }
        tmp = strArray[1];
        length = tmp.length;
        for (let i = 0; i < length; i++) {
            if (!this.isAsciiLetter(tmp.charAt(i))) {
                throw new Error("Invalid Language: \"" + langval + "\"");
            }
        }
        propertyData.addParameter(VCardConstants.PARAM_LANGUAGE, langval);
    }

    private isAsciiLetter(ch: string): boolean {
        if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')) {
            return true;
        }
        return false;
    }

    protected handleAnyParam(propertyData: VCardProperty, paramName: string, paramValue: string): void {
        propertyData.addParameter(paramName, paramValue);
    }

    protected handlePropertyValue(property: VCardProperty, propertyName: string): void {
        const propertyNameUpper: string = property.getName()!.toUpperCase();
        let propertyRawValue: string = property.getRawValue()!;
        const sourceCharset: string = VCardConfig.DEFAULT_INTERMEDIATE_CHARSET;
        const charsetCollection =
            property.getParameters(VCardConstants.PARAM_CHARSET);
        let targetCharset: string =
            ((charsetCollection != null) ? charsetCollection!.values().next().value : null);
        if (!targetCharset) {
            targetCharset = VCardConfig.DEFAULT_IMPORT_CHARSET;
        }

        // TODO: have "separableProperty" which reflects vCard spec..
        if (propertyNameUpper == (VCardConstants.PROPERTY_ADR)
            || propertyNameUpper == (VCardConstants.PROPERTY_ORG)
            || propertyNameUpper == (VCardConstants.PROPERTY_N)) {
            this.handleAdrOrgN(property, propertyRawValue, sourceCharset, targetCharset);
            return;
        }

        if (this.mCurrentEncoding == (VCardConstants.PARAM_ENCODING_QP) ||
            // If encoding attribute is missing, then attempt to detect QP encoding.
            // This is to handle a bug where the android exporter was creating FN properties
            // with missing encoding.  b/7292017
            (propertyNameUpper == (VCardConstants.PROPERTY_FN) &&
                property.getParameters(VCardConstants.PARAM_ENCODING) == null &&
                VCardUtils.appearsLikeAndroidVCardQuotedPrintable(propertyRawValue))
        ) {
            const quotedPrintablePart: string = this.getQuotedPrintablePart(propertyRawValue);
            const propertyEncodedValue: string =
                VCardUtils.parseQuotedPrintable(quotedPrintablePart,
                    false, sourceCharset, targetCharset);
            property.setRawValue(quotedPrintablePart);
            property.setValues(propertyEncodedValue);
            this.mInterpreterList.forEach((v, i, a) => {
                v.onPropertyCreated(property);

            })
        } else if (this.mCurrentEncoding == (VCardConstants.PARAM_ENCODING_BASE64)
            || this.mCurrentEncoding == (VCardConstants.PARAM_ENCODING_B)) {
            // It is very rare, but some BASE64 data may be so big that
            // OutOfMemoryError occurs. To ignore such cases, use try-catch.
            try {
                const base64Property: string = this.getBase64(propertyRawValue);
                try {
                    property.setByteValue(PlatformFactory.getBase64Impl().decode(base64Property));
                } catch (e) {
                    throw new Error("Decode error on base64 photo: " + propertyRawValue);
                }
                this.mInterpreterList.forEach((v, i, a) => {
                    v.onPropertyCreated(property);

                })
            } catch (error) {
                Log.e(this.LOG_TAG, "OutOfMemoryError happened during parsing BASE64 data!");

                this.mInterpreterList.forEach((interpreter, index, array) => {
                    interpreter.onPropertyCreated(property);
                })
            }
        } else {
            if (!(this.mCurrentEncoding == ("7BIT") || this.mCurrentEncoding == ("8BIT") ||

                this.mCurrentEncoding!.startsWith("X-"))) {
                Log.w(this.LOG_TAG,
                    `The encoding \"${this.mCurrentEncoding}\" is unsupported by vCard ${this.getVersionstring()}`)
            }

            // Some device uses line folding defined in RFC 2425, which is not allowed
            // in vCard 2.1 (while needed in vCard 3.0).
            //
            // e.g.
            // BEGIN:VCARD
            // VERSION:2.1
            // N:;Omega;;;
            // EMAIL;INTERNET:"Omega"
            //   <omega@example.com>
            // FN:Omega
            // END:VCARD
            //
            // The vCard above assumes that email address should become:
            // "Omega" <omega@example.com>
            //
            // But vCard 2.1 requires Quote-Printable when a line contains line break(s).
            //
            // For more information about line folding,
            // see "5.8.1. Line delimiting and folding" in RFC 2425.
            //
            // We take care of this case more formally in vCard 3.0, so we only need to
            // do this in vCard 2.1.
            if (this.getVersion() == VCardConfig.VERSION_21) {

                //let builder: Buffer | null = null;
                let builder : StringBuilder | null
                let builderIndex = 0
                while (true) {
                    const nextLine: string | null = this.peekLine();
                    // We don't need to care too much about this exceptional case,
                    // but we should not wrongly eat up "END:VCARD", since it critically
                    // breaks this parser's state machine.
                    // Thus we roughly look over the next line and confirm it is at least not
                    // "END:VCARD". This extra fee is worth paying. This is exceptional
                    // anyway.
                    if (nextLine &&
                        nextLine.charAt(0) == ' ' &&
                        !"END:VCARD".includes(nextLine.toUpperCase())) {
                        this.getLine();  // Drop the next line.

                        if (builder == null) {
                            //builder = Buffer.alloc(1024);
                            builder = new StringBuilder()
                            //builderIndex += builder.write(propertyRawValue, builderIndex, 1024 - builderIndex, "utf8");
                            builder.append(propertyRawValue)
                        }
                        builder.append(nextLine.substr(1))
                        //builderIndex += builder.write(nextLine.substring(1), builderIndex, 1024 - builderIndex, "utf8");
                    } else {
                        break;
                    }
                }
                if (builder != null) {
                    propertyRawValue = builder.toString() //Utf8ArrayToStr(new Uint8Array(builder.slice(0, builderIndex)));
                }
            }

            let propertyValueList = new Array<string>();
            let value: string = this.maybeUnescapeText(VCardUtils.convertStringCharset(
                propertyRawValue, sourceCharset, targetCharset));
            propertyValueList.push(value);
            property.setValues(...propertyValueList);
            this.mInterpreterList.forEach((interpreter, index, array) => {
                interpreter.onPropertyCreated(property);
            })
        }
    }

    private handleAdrOrgN(property: VCardProperty, propertyRawValue: string,
        sourceCharset: string, targetCharset: string): void {
        let encodedValueList = new Array<string>();

        // vCard 2.1 does not allow QUOTED-PRINTABLE here, but some softwares/devices emit
        // such data.
        if (this.mCurrentEncoding!.includes(VCardConstants.PARAM_ENCODING_QP)) {
            // First we retrieve Quoted-Printable String from vCard entry, which may include
            // multiple lines.
            const quotedPrintablePart: string = this.getQuotedPrintablePart(propertyRawValue);

            // "Raw value" from the view of users should contain all part of QP string.
            // TODO: add test for this handling
            property.setRawValue(quotedPrintablePart);

            // We split Quoted-Printable String using semi-colon before decoding it, as
            // the Quoted-Printable may have semi-colon, which confuses splitter.
            const quotedPrintableValueList: Array<string> =
                VCardUtils.constructListFromValue(quotedPrintablePart, this.getVersion());
            quotedPrintableValueList.forEach((quotedPrintableValue, index, array) => {
                let encoded = VCardUtils.parseQuotedPrintable(quotedPrintableValue,
                    false, sourceCharset, targetCharset);
                encodedValueList.push(encoded)
            })
        } else {
            const propertyValue: string = VCardUtils.convertStringCharset(
                this.getPotentialMultiline(propertyRawValue), sourceCharset, targetCharset);
            const valueList: Array<string> =
                VCardUtils.constructListFromValue(propertyValue, this.getVersion());

            valueList.forEach((value, a, b) => {
                encodedValueList.push(value);
            })
        }
        property.setValues(...encodedValueList);

        this.mInterpreterList.forEach((interpreter, a, b) => {
            interpreter.onPropertyCreated(property);
        })
    }

    /**
     * <p>
     * Parses and returns Quoted-Printable.
     * </p>
     *
     * @param firstString The string following a parameter name and attributes.
     *            Example: "string" in
     *            "ADR:ENCODING=QUOTED-PRINTABLE:string\n\r".
     * @return whole Quoted-Printable string, including a given argument and
     *         following lines. Excludes the last empty line following to Quoted
     *         Printable lines.
     * @throws IOException
     * @throws VCardException
     */
    private getQuotedPrintablePart(firstString: string): string {
        // Specifically, there may be some padding between = and CRLF.
        // See the following:
        //
        // qp-line := *(qp-segment transport-padding CRLF)
        // qp-part transport-padding
        // qp-segment := qp-section *(SPACE / TAB) "="
        // ; Maximum length of 76 characters
        //
        // e.g. (from RFC 2045)
        // Now's the time =
        // for all folk to come=
        // to the aid of their country.
        if (firstString.trim().endsWith("=")) {
            // remove "transport-padding"
            let pos = firstString.length - 1;
            while (firstString.charAt(pos) != '=') {
            }
            let builder = new StringBuilder();
            builder.append(firstString.substring(0, pos + 1));
            builder.append("\r\n");
            let line;
            while (true) {
                line = this.getLine();
                if (line == null) {
                    throw new Error("File ended during parsing a Quoted-Printable String");
                }
                if (line.trim().endsWith("=")) {
                    // remove "transport-padding"
                    pos = line.length - 1;
                    while (line.charAt(pos) != '=') {
                    }
                    builder.append(line.substring(0, pos + 1));
                    builder.append("\r\n");
                } else {
                    builder.append(line);
                    break;
                }
            }
            return builder.toString();
        } else {
            return firstString;
        }
    }

    /**
     * Given the first line of a property, checks consecutive lines after it and builds a new
     * multi-line value if it exists.
     *
     * @param firstString The first line of the property.
     * @return A new property, potentially built from multiple lines.
     * @throws IOException
     */
    private getPotentialMultiline(firstString: string): string {
        const builder = new StringBuilder();
        builder.append(firstString);

        while (true) {
            const line = this.peekLine();
            if (line == null || line.length == 0) {
                break;
            }

            const propertyName: string | null = this.getPropertyNameUpperCase(line);
            if (propertyName != null) {
                break;
            }

            // vCard 2.1 does not allow multi-line of adr but microsoft vcards may have it.
            // We will consider the next line to be a part of a multi-line value if it does not
            // contain a property name (i.e. a colon or semi-colon).
            // Consume the line.
            this.getLine();
            builder.append(" ").append(line);
        }

        return builder.toString();
    }

    protected getBase64(firstString: string): string {
        const builder = new StringBuilder();
        builder.append(firstString);

        while (true) {
            const line = this.peekLine();
            if (line == null) {
                throw new Error("File ended during parsing BASE64 binary");
            }

            // vCard 2.1 requires two spaces at the end of BASE64 strings, but some vCard doesn't
            // have them. We try to detect those cases using colon and semi-colon, given BASE64
            // does not contain it.
            // E.g.
            //      TEL;TYPE=WORK:+5555555
            // or
            //      END:VCARD
            let propertyName = this.getPropertyNameUpperCase(line);
            if (propertyName && (this.getKnownPropertyNameSet().has(propertyName) ||
                VCardConstants.PROPERTY_X_ANDROID_CUSTOM.includes(propertyName))) {
                Log.w(this.LOG_TAG, "Found a next property during parsing a BASE64 string, " +
                    "which must not contain semi-colon or colon. Treat the line as next "
                    + "property.");
                Log.w(this.LOG_TAG, "Problematic line: " + line.trim());
                break;
            }

            // Consume the line.
            this.getLine();

            if (line.length == 0) {
                break;
            }
            // Trim off any extraneous whitespace to handle 2.1 implementations
            // that use 3.0 style line continuations. This is safe because space
            // isn't a Base64 encoding value.
            builder.append(line.trim());
        }

        return builder.toString();
    }

    /**
     * Extracts the property name portion of a given vCard line.
     * <p>
     * Properties must contain a colon.
     * <p>
     * E.g.
     *      TEL;TYPE=WORK:+5555555  // returns "TEL"
     *      END:VCARD // returns "END"
     *      TEL; // returns null
     *
     * @param line The vCard line.
     * @return The property name portion. {@literal null} if no property name found.
     */
    private getPropertyNameUpperCase(line: string): string | null {
        const colonIndex: number = line.indexOf(":");
        if (colonIndex > -1) {
            const semiColonIndex: number = line.indexOf(";");

            // Find the minimum index that is greater than -1.
            let minIndex: number;
            if (colonIndex == -1) {
                minIndex = semiColonIndex;
            } else if (semiColonIndex == -1) {
                minIndex = colonIndex;
            } else {
                minIndex = Math.min(colonIndex, semiColonIndex);
            }
            return line.substring(0, minIndex).toUpperCase();
        }
        return null;
    }

    /*
     * vCard 2.1 specifies AGENT allows one vcard entry. Currently we emit an
     * error toward the AGENT property.
     * // TODO: Support AGENT property.
     * item =
     * ... / [groups "."] "AGENT" [params] ":" vcard CRLF vcard = "BEGIN" [ws]
     * ":" [ws] "VCARD" [ws] 1*CRLF items *CRLF "END" [ws] ":" [ws] "VCARD"
     */
    protected handleAgent(property: VCardProperty): void {
        if (!property.getRawValue()!.toUpperCase().includes("BEGIN:VCARD")) {
            // Apparently invalid line seen in Windows Mobile 6.5. Ignore them.
            this.mInterpreterList.forEach((interpreter, a, b) => {
                interpreter.onPropertyCreated(property);
            })
            return;
        } else {
            throw new Error("AGENT Property is not supported now.");
        }
    }

    /**
    * For vCard 3.0.
    */
    protected maybeUnescapeText(text: string): string {
        return text;
    }

    /**
     * Returns unescaped String if the character should be unescaped. Return
     * null otherwise. e.g. In vCard 2.1, "\;" should be unescaped into ";"
     * while "\x" should not be.
     */
    protected maybeUnescapeCharacter(ch: number): string | null {
        return VCardParserImpl_V21.unescapeCharacter(ch);
    }

    /* package */
    public static unescapeCharacter(ch: number): string | null {
        // Original vCard 2.1 specification does not allow transformation
        // "\:" -> ":", "\," -> ",", and "\\" -> "\", but previous
        // implementation of
        // this class allowed them, so keep it as is.
        if (String.fromCharCode(ch) == '\\' || String.fromCharCode(ch) == ';' || String.fromCharCode(ch) == ':' || String.fromCharCode(ch) == ',') {
            return String.fromCharCode(ch);
        } else {
            return null;
        }
    }

    //getter
    protected getVersion(): number {
        return VCardConfig.VERSION_21;
    }

    /**
     * @return {@link VCardConfig#VERSION_30}
     */
    protected getVersionstring(): string {
        return VCardConstants.VERSION_V21;
    }

    protected getKnownPropertyNameSet(): Set<string> {
        return VCardParser_V21.sKnownPropertyNameSet;
    }

    protected getKnownTypeSet(): Set<string> {
        return VCardParser_V21.sKnownTypeSet;
    }

    protected getKnownValueSet(): Set<string> {
        return VCardParser_V21.sKnownValueSet;
    }

    protected getAvailableEncodingSet(): Set<string> {
        return VCardParser_V21.sAvailableEncoding;
    }

    protected getDefaultEncoding(): string {
        return VCardParserImpl_V21.DEFAULT_ENCODING;
    }

    protected getDefaultCharset(): string {
        return VCardParserImpl_V21.DEFAULT_CHARSET;
    }

    protected getCurrentCharset(): string {
        return this.mCurrentCharset!;
    }

    public addInterpreter(interpreter: VCardInterpreter): void {
        this.mInterpreterList.push(interpreter);
    }

    public parse(is: string | number) {

        Log.e(this.LOG_TAG,"in A")


        if (is == null) {
            return
        }

        Log.e(this.LOG_TAG,"in B")
        this.mReader = PlatformFactory.getLineReaderImpl(is);

        this.mInterpreterList.forEach((interpreter, index, list) => {
            interpreter.onEntryStarted()
        })

        Log.e(this.LOG_TAG,"in C")
        // vcard_file = [wsls] vcard [wsls]
        while (true) {
            if (this.mCanceled) {
                //Log.i(LOG_TAG, "Cancel request has come. exitting parse operation.");
                break;
            }
            if (!this.parseOneVCard()) {
                break;
            }
        }

        this.mInterpreterList.forEach((interpreter, index, arr) => {
            interpreter.onVCardEnded()
        });
    }

    public parseOne(is: string | number): void {
        if (is == null) {
            throw new Error("InputStream must not be null.");
        }

        this.mReader = PlatformFactory.getLineReaderImpl(is);

        this.mInterpreterList.forEach((interpreter, index, arr) => {
            interpreter.onVCardStarted()
        })
        this.parseOneVCard();
        this.mInterpreterList.forEach((interpreter, index, arr) => {
            interpreter.onVCardEnded()
        })
    }

    public cancel(): void {
        this.mCanceled = true;
    }
}